package com.`as`.calculator

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

abstract class BasicActivity(private val layout: Int) : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout)

        val editText = findViewById<EditText>(R.id.edit_text)

        listOf<Button>(
            findViewById(R.id.one),
            findViewById(R.id.two),
            findViewById(R.id.three),
            findViewById(R.id.four),
            findViewById(R.id.five),
            findViewById(R.id.six),
            findViewById(R.id.seven),
            findViewById(R.id.eight),
            findViewById(R.id.nine),
            findViewById(R.id.plus),
            findViewById(R.id.minus),
            findViewById(R.id.multiply)
        ).forEach { button ->
            button.setOnClickListener {
                editText.append((it as Button).text)
            }
        }
    }
}